+++
title = "Improving Characters"
description = "Rules for improving characters in 100H."
weight = 150
+++

Characters in 100H advance in one of two ways:

1. By using their skills, characters have a chance to improve those skills.
2. By spending improvement points gained by achieving ambitions or by spending long periods of time practicing and researching, characters can improve themselves.

## Gaining Improvement Points via Practice or Research

During downtime the players may improve their characters.
For every three month period of dedicated practice or research a character may gain 1 Improvement Point.

## Improving Skills Through Use

The first time a character succeeds on a normal or hard test which uses a skill, place a mark beside that skill.
At the completion of a quest or when beginning an interlude - whenever there's a sensible break in the quest to reflect and take time to recover - players should roll 1d100 for each skill they marked during play.
If the result of the roll is _greater than_ the bonus of the skill they're rolling for, they have improved it.
In this case, add three to the skill bonus.
This is the character's new skill bonus.

_Every time_ you fumble a test using a skill, add 1D4 to the skill.
This represents a character gleaning new understanding from failure.

There is no limit to the bonus a skill can reach.

{{% example "Improving Skills" %}}
After their first adventure, Karacter has successfully used the following skills: leadership, first-aid, scrounging, and wrestling.
The player rolls 1D100 for each skill:

+ Karacter rolled a 55 against Leadership (50%), and improves it by 3 to 53%.
+ Karacter rolled a 82 against First Aid (20%) and improves it to 23%.
+ Karacter rolled a 42 against Scrounging (45%) and does not improve it.
+ Karacter rolled a 40 against Wrestling (30%) and improves it to 33%.

{{% /example %}}

## Spending Improvement Points

Characters can spend improvement points to improve skills, characteristics, or purchase new knacks.

A player can choose to spend one improvement point to increase on skill bonus by 5 points.

A player can choose to spend three improvement points to increase one characteristic by one point.
The maximum a human character can increase a characteristic to is 21.
For non-humans, the maximum for a characteristic is equal to the maximum possible starting score for the characteristic plus three.

A player can spend one improvement point per magnitude to acquire or improve an knack.

{{% example "Spending Improvement Points" %}}
After their first adventure, Karacter has five IP to spend.

Looking back over things, Karacter decides to spend three IP to improve their BOD from 13 to 14.
Next, Karacter spends one IP to increase the magnitude of their Vigor knack to 2.
Finally, Karacter spends their last IP improving their marksmanship skill by 5% to 40%.
{{% /example %}}