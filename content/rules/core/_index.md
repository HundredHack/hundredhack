+++
title = "Core Rules"
description = "The core rules of the HundredHack (100H) system."
weight = 10
+++

<p style="text-align:center"><em/>
Inspired by David Black's incredible The Black Hack and Newt Newport's unbelievable Open Quest.
</em></p>

----

# What’s This?

The Hundred Hack (100H) is a traditional tabletop roleplaying game, played with paper, pencils and dice - it uses OpenQuest as a base.
But it adds and takes away elements to make it a distinct streamlined flavor of the original roleplaying game.

{{% children style="h5" description="true" depth="3" sort="weight" %}}