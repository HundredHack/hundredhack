+++
title = "Knacks"
description = "List and explanation of knacks, the special abilities characters possess in 100H"
weight = 130
+++

Knacks are special abilities characters can possess.
The knacks are purposefully written to be generic, allowing them to be flavored however makes sense for your character.

As noted in the [character creation section](character-creation.md), characters begin play with ten points of magnitude worth of knacks.
Players may select any combination of knacks at any magnitude, so long as the total combined magnitude of the knacks chosen does not exceed 10.

The maximum magnitude of a knack is limited by a character's POW.
For example, a character with a POW of 12 could not have any knacks whose magnitude was 13 or higher.
Note that this limits the maximum magnitude of any given knack, not the total magnitude of knacks a character can acquire.

{{% excerpt %}}
Unless otherwise stated all knacks have the following traits.

+ Base magnitude is one.
+ They have variable magnitude.
  This means that the magnitude of the knack starts from the stated magnitude and then can be used at a higher magnitude, if the character has it, giving an increase in the effect of the knack.
  The maximum magnitude that a character can learn is equal to their Power score.
+ Range is nearby.
+ Duration is ten minutes.

Other traits used by knacks are detailed below.

+ **Area (X):** The knack affects everything within the specified distance, centered on a target point.
+ **Concentration:** The knack's effects will remain in place so long as the character continues to concentrate on it.
+ **Instant:** The knack's effects take place instantly and do not have a duration unless specified.
+ **Magnitude (X):** The strength and power of the knack and the minimum  PP that must be spent to use it.
+ **Non-Variable:** The knack may only be used at the stated magnitude.
+ **Permanent:** The knack's effects remain in place until they are dispelled or dismissed.
+ **Passive:** The knack is always in effect and cannot be dispelled or dismissed.
+ **Prerequisites:** This knack has one or more prerequisite conditions which must be met before the knack can be acquired.
+ **Resist:** The knack's intended effects do not succeed automatically and the target may make an opposed test to avoid them entirely.
+ **Touch:** Touch knacks require the character to actually touch their target for the knack to take effect, using a close combat test to make contact.
+ **Trigger:**  The knack's effects take place when a certain condition is triggered and do not have a duration unless specified.

{{% /excerpt %}}

{{% knacks linkify="true" include="core" %}}
