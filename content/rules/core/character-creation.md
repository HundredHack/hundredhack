+++
title = "Character Creation"
description = "Rules and guidance on character creation in 100H"
weight = 110
+++

Characters are creating by following these steps:

1. [Determining characteristics](#determining-characteristics)
2. [Calculating attributes](#calculating-attributes)
3. [Choosing skills](#choosing-skills)
4. [Determining wealth and buying equipment](#determining-wealth-and-buying-equipment)
5. [Setting ambitions](#setting-ambitions)

## Determining Characteristics

Characters in 100H have three characteristics: Body (BOD), Intellect (INT), and Power (POW).

Body represents a character's physical prowess, including their strength, agility, toughness, hand-eye coordination, etc.
Intellect represents a character's mental prowess, including raw intelligence, cunning, intuition, etc.
Power represent's a character's force of will and (if applicable) metaphysical aptitude (raw magic, psychic aptitude, etc).

Each characteristic has a starting value of 5.
At the beginning of play you have 20 points to distribute across the three characteristics however you like.

A 10 in a characteristic is the average for a human - higher scores are above average - and a 20 is the absolute upper bound of a human not affected by special abilities or conditions (magic, cybertech, etc).

Characteristics give a base goal for tests as 3x the characteristic.

Declare _what_ you're trying to accomplish **and** _how_.
The referee will determine if your action requires a test and which characteristic is most appropriate.

Then you'll try to roll under the appropriate goal.

{{% example "Determining Characteristics for Karacter" %}}
To determine Karacter's characteristics, we start with 5 each in BOD, INT, and POW:

+ BOD: 5
+ INT: 5
+ POW: 5

We want Karacter to be more physical and personally powerful than mentally capable, so we assign our starting characteristics like this:

+ BOD: 13 (`5+8`)
+ INT: 9 (`5+4`)
+ POW: 13 (`5+8`)

At the beginning of play Karacter is stronger, tougher, more agile, and more personally powerful than most people, but not the brightest of folks - though not unintelligent by any means.

Because the base goal for tests is 3x a characteristic, Karacter's stat block ends up looking like this so far:

+ BOD: 13 (39%)
+ INT: 9 (27%)
+ POW: 13 (39%)

{{% /example %}}

## Calculating Attributes

+ **Damage Bonus (DB):** When attacking, characters add +1 damage for every four points of their relevant characteristic.
  For example, if attacking with an axe and a BOD of 15, the character would have a +3 damage bonus; if shooting a bow with a BOD of 12, a +3 damage bonus; if dueling as astral projections with a POW of 16, a +4 damage bonus, and so on.
+ **Hit Points (HP):** Add your character's BOD and POW, then divide by two (round up) to determine HP.
+ **Power Points (PP):** Add your character's INT and POW, then divide by two (round up) to determine PP.

{{% example "Determining Attributes for Karacter" %}}
Having rolled the characteristics for Karacter we can calculate their attributes:

+ Damage bonus: When attacking with weapons, Karacter has a DB of +3.
+ Hit Points: With a BOD of 13 and a POW of 13, Karacter's HP is 13.
+ Power Points: With an INT of 9 and a POW of 13, Karacter's PP is 11.

{{% /example %}}

## Choosing Skills

Skills are things a character specializes in doing.
At the beginning of play, a character has 250 percentage points to spend on skills.
In Hundred Hack there is no defined skill list, but here are a few examples for inspiration:

> Dodge, Archery, Fencing, Culture (Persian), Lore (Alchemy), Athletics, Sculpting, Driving, Influence, Mechanisms, Streetwise, Trade.

The highest relevant skill bonus is always added to the character's goal for a test.

{{% example "Choosing Skills for Karacter" %}}
We want Karacter to be something of a fighter, a leader of troops who can inspire followers and aid them.
We choose to allocate the 250 points like this:

+ Calligraphy (25)
+ Leadership (50)
+ Marksmanship (35)
+ First Aid (20)
+ Scrounging (45)
+ Wrestling (30)
+ Analysis (45)

{{% /example %}}

## Selecting Knacks

Characters begin with 10 points of magnitude worth of knacks, special abilities detailed in the [next chapter](knacks.md).
See that chapter for more details on specific knacks.

{{% example "Selecting Knacks for Karacter" %}}
As Karacter intends to be a warrior and leader of warriors, we'll take the following knacks:

+ [Thunder's Voice](../knacks#knack-thunders-voice) at magnitude 3, helping Karacter to inspire soldiers, intimidate enemies, and otherwise operate as a leader.
+ [Coordination](../knacks#knack-coordination) at magnitude 2, helping Karacter to act more quickly and with more surety in danger, or to inspire the same in their followers.
+ [Bearing Witness](../knacks#knack-bearing-witness) at magnitude 3, helping Karacter to discover the truth, an important quality in anyone responsible for other people.
+ [Detect Enemy](../knacks#knack-detect-x) at magnitude 1, helping Karacter to suss out ambushes or hiding enemies at short range.
+ [Vigor](../knacks#knack-vigor) at magnitude 1, helping Karacter to stiffen either their own health in the face of battle, or grant a small boon to a warrior.

{{% /example %}}

## Determining Wealth and Buying Equipment

In 100H, characters begin with a default wealth of Average.
You can, instead, opt to begin play with three additional [improvement points](../improving-characters) per wealth level you forego.

| IP | Description | Social Class               | Usage Die |
|:--:|:-----------:|:--------------------------:|:---------:|
| 1  | Destitute   | Beggars and serfs          | D4        |
| 4  | Poor        | Freemen laborers           | D6        |
| 7  | Average     | Freemen with a trade       | D8        |
| 11 | Well-off    | Minor merchants, officials | D10       |
| 15 | Wealthy     | Merchants, minor nobility  | D12       |
| 20 | Rich        | Kings, powerful nobles     | D20       |

Whenever you make a sizable purchase, _including buying starting equipment_, roll your usage die.
On a roll of a 1, your usage die decreases in size as normal.
Once your usage die has been fully expended, your wealth drops by one rank and you gain a new usage die appropriate to your new wealth.

Whenever you make a sizable sale, roll your usage die.
If the result of the roll is the maximum for that die your usage die increases one step in size, up to the maximum for that wealth level.

Later, when [improving your character](#improving-characters), you can spend a number of improvement points equal to the difference between your current wealth and the desired rank.

{{% example "Improving Wealth" %}}
Karacter started play with Average wealth and wants to increase it to Well-Off.
The difference in IP costs is `11 - 7`, so Karacter would pay 4 IP to increase their wealth.
If Karacter wanted to instead increase their wealth from Average to Wealthy, they would pay 8 IP.
{{% /example %}}

### Encumbrance

A character can carry a number of items equal to their BOD with no issues.
Carrying over this amount means they are encumbered and all tests are one step harder - they can also only ever move to somewhere nearby in a moment.
They simply cannot carry more than double their BOD.

### Armor

Armor provides protection (AP) by reducing all incoming damage.
When taking damage from an attack reduce the incoming damage by the AP.
However, heavier armors impose penalties, making some tests one step harder.

Armors also have a usage die which must be rolled after every combat in which damage was taken that the armor did not completely absorb.

| Type   | AP | Usage Die | Affected Tests                  |
|:------:|:--:|:---------:|:-------------------------------:|
| Light  | 3  |     D6    | None                            |
| Medium | 5  |     D8    | Athletics and fatigue           |
| Heavy  | 7  |     D10   | Athletics, fatigue, and stealth |

### Weapons and Shields

Weapons and shields have sizes - when a character uses their reaction to parry an attack, they can negate all of the damage on a successful parry if their weapon or shield is the same size; if the parrying weapon/shield is one size smaller it blocks half the damage; if the parrying weapon/shield is more than one size smaller it blocks none of the damage.

Weapons and shields have a damage die which is rolled to determine how much HP damage is inflicted on the target.
Remember that AP and parrying can reduce incoming damage to zero.

In addition to damage, weapons and shields also have a usage die.
For weapons, the usage die is rolled after every combat where the weapon fails to damage an enemy because all of the damage was negated by AP.
For shields, the usage die is rolled after every combat where the shield is successfully used to parry an attack against a weapon.

| Weapon           | Usage Die | Damage |
|:-----------------|:---------:|:------:|
| Small 1H Weapon  |    D8     |   1D6  |
| Medium 1H Weapon |    D8     |   1D8  |
| Large 2H Weapon  |    D8     |   2D6  |
| Small Shield     |    D6     |   D4   |
| Medium Shield    |    D8     |   D6   |
| Large Shield     |   D10     |   D8   |
| Huge Shield      |   D12     |   D10  |

### Miscellaneous Equipment Table

| Item                     | Usage Die |               Notes               |
|:-------------------------|:---------:|:---------------------------------:|
| Backpack                 |     -     |           Carry +2 extra          |
| Flask of Oil             |    D6     |                 -                 |
| Tools                    |     -     | Related tests are one step easier |
| Lantern                  |     -     |                 -                 |
| Handheld Mirror          |     -     |                 -                 |
| Preserved Rations        |    D8     |                 -                 |
| Fresh Rations            |    D4     |                 -                 |
| 50' Rope                 |     -     |                 -                 |
| Small Sack               |     -     |                 -                 |
| Large Sack               |     -     |                 -                 |
| Flint & Steel            |     -     |                 -                 |
| Torches (6)              |    D6     |     Each Torch has a usage die    |
| Wineskin                 |    D6     |                 -                 |
| Assorted Common Herbs    |    D8     |                 -                 |
| 10' Pole                 |     -     |                 -                 |
| Quiver of Arrows / Bolts |    D10    |                 -                 |

{{% example "Determining Wealth and Buying Equipment for Karacter" %}}
We decide to have Karacter forego one level of wealth and begin play as Poor to gain an addition three improvement points immediately.

This means picking up starting equipment is going to be a bit dicey.

Karacter needs heavy armor, rations, a wineskin, a longbow, arrows, a lantern, oil, and herbs - but with a Wealth of Poor, Karacter may have their usage die reduced by one or more steps - maybe even becoming destitute!

We roll for each item:

+ Heavy Armor (4 - usage die does not reduce)
+ Rations (6 - no change)
+ Wineskin (1 - usage die is reduced to 1D4!)

At this point, Karacter elects not to continue purchasing starting equipment, picking the negligible cost of a quarterstaff instead of the initially-preferred longbow etc.
This alters our plans for Karacter a bit, but nothing we can't work around.

{{% /example %}}

## Setting Ambitions

At the start of play, give your character two long term ambitions and one short term ambition.

Long term ambitions are things that are life ambitions, only achievable over the period of a linked set of adventures, commonly known as a ‘campaign'.
Short term ambitions are usually relevant to the adventure currently being played, and are determined near the beginning of the session by the players.

Achieving ambitions help you earn improvement points, which you can spend to [improve your character](#improving-characters):

+ Every time an ambition is brought into play in a concrete way, the character earns one improvement per session.
+ Short term ambitions are removed at the end of the session, and if completed earn an additional two improvement points.
+ When a long term ambition is finally achieved it is removed from the player's character sheet and the character earns five improvement points.

{{% example "Setting Ambitions for Karacter" %}}
For Karacter we'll set the following ambitions:

+ Short: Pass the trials required to join the local chapter of the Wardens, a guild of mercenaries specializing in guard duty.
+ Long: Become a Chapter Master of the Wardens.
+ Long: Become the heroic subject of an epic poem.

{{% /example %}}