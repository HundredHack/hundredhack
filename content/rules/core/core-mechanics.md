+++
title = "The Core Mechanics"
description = "The primary mechanics used by 100H."
weight = 100
+++

## Tests

In 100H ambiguous actions are resolved by making tests.

Before any test, the player first describes _what_ the character is trying to accomplish and _how_; this establishes the **intent and approach** and _only after_ the intent and approach are established will the referee decide whether or not a test is necessary.
Referees should not call for tests unless the approach specified by the player could either fail or succeed to fulfill the intent and even then only if failure will be of some importance.

If the referee decides that a test is called for, they will then specify the appropriate characteristic.
The player should add their highest relevant skill bonus to their goal.
In cases where it is ambiguous if a skill should apply, the referee will decide.

In order to successfully pass a test a player must roll **below** their goal for the specified characteristic on a d100.

{{% example "Actions and Tests: Locked Door" %}}
Warrior is attempting to get past a locked door by kicking it down in several scenarios:

1. Warrior reaches an abandoned warehouse on the outskirts of town to look for clues but the side door is locked.
   Even though it is possible that Warrior's first attempt to break down the door might fail in this scenario the referee decides there are no meaningful consequences to that failure, nor to retrying until she _does_ kick the door down.
   The referee declares that Warrior successfully kicks the door in.
2. Warrior is chasing a fleeing bandit who has slipped into the building and barred the door behind him to buy himself more time to escape.
   In this scenario not only can Warrior fail to kick in the door but each failed attempt gives the fleeing bandit more time to escape.
   The referee decides that a test is required and directs Warrior to test against her BOD, adding any relevant bonuses she may have.
3. Warrior is locked into a steel vault and needs to escape.
   In this scenario failure to kick open the door means that Warrior remains locked in the vault.
   However, the referee notes that the vault door is nearly a foot thick and that Warrior's stated approach - kicking down the door - is simply impossible.
   The referee declares that Warrior kicks the steel door but fails to open it.

{{% /example %}}

Some tests are easy, doubling the chance of success.
Other tests are hard, halving the chance of success.

{{% example "Tests: Easy and Hard" %}}
Teacher is attempting to convince the guard to share some gossip using several different approaches:

1. Teacher wants to share false gossip about the topic to prompt the guard to share his own knowledge.
   The referee decides that this approach _could_ work, but isn't particularly more or less likely to do so.
   The referee therefore calls for an INT test to pull off the minor deception.
2. Teacher wants to approach the guard with a smoke and a friendly smile before mentioning the topic of gossip off-handedly.
   The referee knows that the guard is young and slightly ill disciplined  - and decides that the guard is particularly susceptible to this method.
   The referee therefore calls for an easy INT test, allowing Teacher to double his goal for this test.
3. Teacher wants to intimidate the guard into sharing the gossip by being physical and not-quite threatening him.
   The referee knows that the guard is young, slightly ill disciplined, and has a lot to prove - and therefore determine that this method is particularly unlikely to get the fulfill the desired intent.
   The referee therefor calls for Teacher to make a hard INT test, forcing Teacher to cut his goal for this test in half.

{{% /example %}}

If the result of the test is less than 1/10 the test's goal, it is a critical success - the _best possible_ result based on the character's intentions.

If the result of the test is 100, it is a fumble - the _worst possible_ result based on the character's intentions.

### Opposed Tests

Opposed tests are made by both sides who are in direct competition with each other.
Both characters make the tests as normal and the results depend on how well both sides do:

+ **One Side Succeeds:**
  If one side succeeds their test and the other fails then the successful side has won the opposed test.
+ **Both Characters Succeed:**
  If both sides succeed then whoever rolled the highest in their test wins the opposed test.
  However, if one side rolls a critical while the other rolls an ordinary success then the side that rolled the critical wins.
+ **Both Sides Fail:**
  Whoever rolled the highest without fumbling in their test wins the opposed test.
  In the case of ties for both the Player wins.
  If the action is player-against-player, the defender wins.

{{% example "Opposed Tests" %}}
Teacher and Spy are vying for the favor of the crowd during a debate.
Spy relies on their natural charm and skill at persuasion to sway the crowd, rolling against a goal of 80%.
Teacher draws on their deep experience and leverages the crowd's emotions through recalling their historic heroines and bringing to mind their greatest triumphs and struggles, making their test easy and rolling against a goal of 140%.

Here are a few ways this could play out, depending on the results of each characters' test:

1. Spy rolls an 87, failing, and Teacher rolls an 83, succeeding.
   Teacher wins over the crowd.
2. Spy rolls a 68, succeeding, and Teacher rolls a 32, succeeding.
   Because both characters succeeded at their test and Spy's roll was higher, Spy wins over the crowd.
3. Spy rolls a 41, succeeding, and Teacher rolls a 12, critically succeeding.
   Even though both character succeeded Teacher had a critical success and therefore wins over the crowd.
4. Spy rolls a 91, failing, and Teacher rolls a 100, fumbling.
   Even though both characters failed, Teacher actually fumbled and therefore Spy wins over the crowd.

{{% /example %}}

## Usage Die

Some special abilities and items will specify that they have a usage die.

Immediately after using an ability with a usage die, or one minute after using an item with one, roll the appropriate usage die.
If the roll is a 1 then the usage die is downgraded to the next lower die in the following chain:

> **D20 > D12 > D10 > D8 > D6 > D4**

When you roll a 1 on a D4 the item or ability is expended and the character has no more uses of it left.

{{% example "Usage Die" %}}
After Burglar uses herbs from his kit to brew a tea to make the shopkeep more susceptible to his influence he must roll the usage die for his herbs, a D8.
Burglar rolls the D8 which lands on a 1, meaning that one use from his herbal kit is gone.
The kit's usage die is reduced by one step to a D6, meaning the next time it is used Burglar will roll that dice instead.

If instead Burglar had rolled a 2 or higher the usage die would remain a D8.
{{% /example %}}
