+++
title = "Example Characters"
description = "Pregenerated characters using the 100H system"
weight = 30
+++

Below are the example characters alluded to in the examples in the rules text.
Each of these characters is ready to play and could be used in a one shot.

{{% characters %}}
