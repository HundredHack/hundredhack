+++
title = "Disease"
description = "Optional mechanics for and examples of diseases in HundredHack."
+++

Every type of disease has the following information detailed:

{{% disease _explanation %}}

Poisoned characters make an opposed test against the poison's potency:

+ **Disease Succeeds, Character Fails:** If the disease succeeds its potency test and the character fails their resistance test, the disease has its full effect.
+ **Character Succeeds, Disease Fails:** If the character succeeds their resistance test and the disease fails its potency test, the disease has no effect.
+ **Both Disease and Character Succeed:** Whoever rolled the highest in their test wins.
+ **Both Disease and Character Fail:** Whoever rolled the highest in their test wins.

The effects of the disease cannot be removed or healed until the disease itself has been neutralized or has dissipated in the victim's system.
HP damage caused by disease will not automatically heal – it must be healed through magical or natural healing.

Diseases will progress if a character does not resist its effects.
Once the first opposed test is failed by the victim, they will have to make an additional opposed test (after an amount of time determined by the disease's delay statistic).

If the victim succeeds this second opposed test, he has overcome the worst of the disease and will no longer suffer its effects, other than remaining hit point damage, after a while.
Use the disease's delay statistic to determine how long this takes.

If the victim fails this second opposed test, he falls deeper into the disease.
Apply all of the disease's effects again to the character.
Once the delay period has elapsed once more, the victim will have to make a third opposed disease test, and so on.

## Disease List

The following list of diseases is _non-exhaustive_.

{{% diseases linkify="true" exclude="example" %}}
