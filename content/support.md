+++
title = "Support Us"
description = "Ways to support the development team"
+++

More than anything, we're happy to have your feedback and engagement.
If you're able to [file an issue](https://gitlab.com/HundredHack/hundredhack/issues/new) we appreciate it - but just [dropping us an email](incoming+HundredHack/hundredhack@gitlab.com) is a totally valid way to engage, too.

If you yell about problems with our work, all we ask is to get looped in on the criticisms so we can look at fixing the work. :)

The very best thing you can do for anyone whose work you appreciate is to talk about it and share it with other folks.

Cheers!