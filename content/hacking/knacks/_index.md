+++
title = "Knacks"
description = "Guidance for creating, modifying, and using knacks in 100H"
weight = 100
+++

Knacks were included in 100H's core rules as a way to generically provide special powers and abilities to characters without specifying a setting or how each power manifests.
The core knacks are therefore reasonably generic and can fit many different settings but for this same reason they lack some distinctive flavor.

The core rules leave it up to the players and referee to add whatever color makes sense for each knack based on the character and the setting.

For example, in the case of the block sense knack, which can be used to blind an enemy, the knack's mechanical definition is this:

{{% knack block-sense %}}

This does not include any details on _how_ the character blinds the enemy.
Depending on the setting and game, it could be any of these, or something else entirely:

+ The character throws glass dust into the opponent's face
+ The character casts a spell that causes the opponent's eyes to well up with painful ichor
+ The character uses a flare to rob the opponent of sight temporarily
+ The character unleashes a small swarm of gnats which attack the opponent's face, causing them to squeeze their eyes shut in defense

In this sense the included knacks are flexible to your needs.

The following sections detail more ways to use, alter, or replace knacks in 100H:

{{% children style="card" description="true" depth="1" sort="weight" %}}