+++
title = "Hacking Knacks"
description = "Guidance for using and modifying knacks in 100H"
weight = 10
+++

The knacks included in the [core rules](../../../rules/core/knacks) are universal, general, and can fit into many settings and games.
On their own, the knacks don't provide too much flavor or system-specific narrative weight; this is by design.

What follows is some guidance on how you can use knacks at your table, in your setting, or with your own game.

## Explaining a Knack

Knacks, by virtue of being freeform, can leave players and the referee foundering a little to explain _how_ a character is able to do the thing the knack specifies.
While you _could_ leave this ambiguous, undefined, or change it every time, this will almost certainly have a negative impact on suspension of disbelief and investment in the fiction for at least some of the players.
A better approach is to define how a knack works on at least a character-by-character basis, keeping in mind that the abilities are largely limited by the availability of power points.

You do _not_, generally, want to explain a knack in a way that doesn't fit with being limited use per day; that is, you want to  associate the mechanic to the fiction sensibly.

For example, using the [block sense](#knack-block-sense-sense) knack costs 3PP per use and blinds the target for 10 minutes.
Explaining this as poking the target in the eyes doesn't quite make sense - _why can't I just do that all the time? Why am I limited by my PP?_
Those questions are indications either that the explanation isn't a good fit or that the explanation is still missing something.
If instead the explanation is that your character is able to preternaturally predict the enemy's movements and thus blind them with a quick strike, it's functionally the same thing (and mechanically, too!) - except the explanation doesn't break the fiction at the table and feels more limited.

Keep in mind that the _feel_ of the explanation **does** matter, especially at the table.
Even slight variations on explanations, as above, can completely change whether or not the knack feels like a special ability.

{{% knack block-sense %}}

For systems that have magic, psychic powers, cybernetics, or other sorts of sources for powers, explaining knacks will draw heavily from those pieces of the setting.
For systems that lack those things, or where they are more on the fringes, knacks _will_ require more work and forethought.

For example, using the [pierce](#knack-pierce) knack means that a character is (temporarily) better able to overcome armor when attacking.
If the pierce knack was used on a firearm you could explain it as only having a limited number of armor piercing rounds.
If used on a melee weapon you could explain it as a burst of adrenaline enabling your character to more accurately / forcefully attack the enemy.
In low/no supernatural settings you'll probably want to explain the knacks on a character-by-character basis or provide default explanations [more scoped to the particular setting](../hacking-knacks#narrowing-a-knack).

{{% knack pierce %}}

## Gating Knacks

In the core rules there's no mechanical limit on which knacks a character can learn and when (provided they have the IP to spend).
In your games or setting you may want to require that characters have to pass certain requirements (narrative, mechanical, or both) to acquire or improve knacks.

For example, you might rule that the [pierce](#knack-pierce) knack is only available to a particular guild of weaponsmiths who imbue weapons with a magically sharp edge.
Learning to use the knack yourself might require that you build a relationship with them and convince them to train you.
Alternatively, you might just be satisfied with buying a weapon from them imbued by the knack which you can then use yourself.
Note that the limit in this case is in the fiction - you need to find someone to learn the knack from and can't just spontaneously discover it.

In general, gating knacks behind requirements in the fiction is useful and reinforces the mechanics on the setting and the setting on the mechanics, _when executed fairly and well_.
The downside, of course, is being forced to do things you may not want for your character in order to acquire abilities you think they should have.
Unfortunately, solving the problem of compromise, communication, and expectation management is slightly outside the scope of this document on knacks.

Another (non-exclusive) option is to gate knacks behind mechanical requirements in the form of the prerequisite trait.
None of the knacks included in the core rules include a prerequisite because prerequisites should almost always be setting/game specific.

For example, if added the trait _Prerequisite (Killed an opponent in a judicial duel for each point of magnitude)_ to the Pierce knack, that may not even make sense in settings where there are no judicial duels and it would be a strange gate in a modern urban fantasy setting too.

You can also combine the mechanical and narrative requirements for knacks.
Using the previous example of learning the knack from the Weaponsmith Guild, you might rule that to gain the first point of magnitude requires a 50% bonus to both your relationship with the guild _and_ in a smithing skill.
You could then either gate higher magnitudes of the knack or just gate that first point, depending on your game's context.

{{% notice warning  %}}
When gating a knack you'll want to make sure it's applied to everyone taking that knack, not applied to characters differently.
If a character needs a 50% bonus in a skill to get the knack, so should everyone else.

Failing to heed this will almost definitely cause problems at the table.

A caveat to this is if there are multiple ways to learn a knack or multiple explanations for a knack.
However, this is an advanced implementation of knacks that adds a lot of complexity you may or may not find valuable for your game.
{{% /notice %}}

## Combining Knacks

Sometimes, no existing knack does exactly what you want.
While you can absolutely [write a new knack](../writing-knacks) to meet the need you should look first to simply packaging knacks together.

Returning to the [pierce](#knack-pierce) and [block sense](#knack-block-sense-sense) knacks you could combine these together to be an ability to make a weapon deafen the enemy and ignore armor in the two following knacks:

{{% knack storm-javelin %}}

{{% knack sonic-cannon %}}

Note that the explanation and prerequisites are different for each but the effects are exactly the same - further, note that they match mechanically with the knacks that were combined _and_ that the magnitude is the combination of both.

The combined knack doesn't include a way to deafen more than one target or increase the duration and this is on purpose.
The [block sense](#knack-block-sense-sense) knack is magnitude 3 and non-variable, meaning every time a character wants to use it on a target they must spend 3PP.
Allowing the combined knack to deafen every target would break the existing balance.
Instead, we note that additional magnitude increase the AP only.

Alternatively, we could hack the knack a bit to maintain some balance.
Let's continue to examine the sonic cannon knack and see if we can hack it up a bit.

## Hacking a Knack

We have the basis of a good combined knack in the sonic cannon but for mechanical reasons it only deafens the first target.
But what if we wanted it to affect every target the weapon is shot at?
We'll have to hack it up to make things reasonable.

The first option is to allow it to effect an additional target for every three additional magnitude, keeping even with the existing knacks:

{{% knack sonic-cannon-multi-shot %}}

This is a bit clunky though.
What if, instead, we made it a single shot item but increased the amount of damage it ignores?

{{% knack sonic-cannon-single-shot %}}

This example simulates more of a special round than a clip of them.
It's reasonably powerful, but only lets you ignore the armor on a _single_ attack when it normally lasts for 10 minutes.

There's tons of other ways you might hack this up: you could reduce the duration of the deafening and piercing to one minute, for example:

{{% knack sonic-cannon-one-minute %}}

You could alter the knack entirely, such that failure to resist makes the target deaf and reduces their armor against your follow up attacks:

{{% knack sonic-cannon-on-effect %}}

Note that this knack costs _more_ than the initial knacks combined and that increasing the AP reduction is also more expensive - this is because it also changes the pierce knack's effect by allowing _all_ attacks to ignore that AP, not just this one weapon.

{{% notice note %}}
Make sure that combining the knacks in never costs the same as the two knacks unless the effects are suitably weakened.
Otherwise, you make taking the lone knack a mechanically anti-optimal choice.
{{% /notice %}}

### Narrowing

The universal knacks included in the core rules have few limitations.
The [pierce](#knack-pierce) knack can be applied to a different weapon every time it is used, for example.
In your game or setting you may want to limit knacks further.

This is separate from [gating](#gating-knacks), which puts limits on actually acquiring knacks.
Narrowing instead makes the knacks less broadly applicable and should usually include a reduction in magnitude as well.

For example, if the [skybolt knack](#knack-skybolt) was amended to only be usable during thunderstorms you might reduce the magnitude by 1 - the times when you can use that knack are now severely limited.

{{% knack skybolt %}}

{{% knack skybolt-thunderstorm %}}

Similarly, if you limited the [fanaticism knack](#knack-fanaticism) to only work if the target shares your faith, you might also reduce the magnitude by 1.
Alternatively, you might slightly bump the bonus but keep the magnitude the same:

{{% knack fanaticism %}}

{{% knack fanaticism-shared-religion-cheaper %}}

{{% knack fanaticism-shared-religion-better %}}

{{% notice note %}}
When improving the effect of the knack after narrowing it, be careful not to make it so much more powerful that it wouldn't make sense to take the non-narrowed version.
In general, reducing the cost should be slightly superior to increasing the effect.

This helps because the action economy and stacked effects problems are less exacerbated by cheaper knacks than by more powerful ones.
{{% /notice %}}

In general, narrowing knacks can help to distinguish and specialize knacks for characters and settings.
When used with care and combined with a good explanation this technique can turn one core knack into numerous flavorful options.

## Conclusion

Knacks in 100H are _designed_ and _intended_ to be hacked on.
You can definitely use them straight out of the rules but they also exist for you to modify, remix, and make your own to fit at your table or in your setting.

The main thing to remember when hacking knacks is that they have to make sense in the context of your game and your players.
Sometimes, how a knack _feels_ is more important than whether or not is actually unbalanced.

Trust your instincts and above all feel free to experiment, try things out, and remix them if they're absolutely broken for you.
You'll get a pretty good feeling for whether or not a hacked knack makes sense for you as you go along.