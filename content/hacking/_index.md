+++
title = "Hacking"
description = "Guidance for modifying and using the 100H system"
weight = 200
+++

100H is _specifically_ intended to be hacked up, remixed, altered, and used whole cloth to suit the needs of your table and game.
However, it's not always obvious how to go about using the existing rules, modifying them, or creating alternatives.

The following articles should help to address those concerns - if there's anything you'd like to see covered that isn't, please [file an issue](https://gitlab.com/HundredHack/hundredhack/issues/new) or [email us](incoming+HundredHack/hundredhack@gitlab.com).

{{% children style="h5" description="true" depth="1" sort="weight" %}}