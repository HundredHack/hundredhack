+++
title = "HundredHack"
description = "The landing page for the HundredHack project and sub projects."
+++

This is the home page for HundredHack, a tabletop role-playing game, and related projects.
Here you'll find rules (core, optional, alternate, and collections), settings, encounters, and other tools and resources for players and GMs alike.

We're under heavy construction and fast iteration right now, so we apologize if things seem to be rapidly changing under your feet.